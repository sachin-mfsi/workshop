#!/usr/bin/python3
import cgi
import configparser  # To read constants file
import copy
import csv  # To parse CSV file data
import datetime
import os
import re
import shutil  # To save uploaded file to local directory
import traceback

import mysql.connector
from mysql.connector import Error

# Displays CSV upload page on GET request.

if os.environ["REQUEST_METHOD"] == "GET":
    print("Content-type: text/html\n")
    f = open("./template/upload.html", encoding="utf-8")
    print(f.read())
    f.close()

# Upload the data to database on getting POST request.

elif os.environ["REQUEST_METHOD"] == "POST":
    print("Content-type: text/html\n")
    form = cgi.FieldStorage()
    datafile = None
    timestamp = datetime.datetime.now().strftime("-%d-%m-%j-%X")
    filename = form["uploadFile"].filename.split('.')
    filename = filename[0] + timestamp + '.' + filename[-1]

    # Saving uploaded file to local directory

    if "uploadFile" in form:
        try:
            outpath = os.path.join("./uploadedfiles/", filename)
            with open(outpath, "wb") as fout:
                shutil.copyfileobj(form["uploadFile"].file, fout, 100000)
        except:
            print("Can not save the file")
    try:

        with open(outpath, encoding='utf-8') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';')
            error_arr = []

            # Checking required columns in submitted file

            required_columns = ["EmpID", "Name", "CreatedBy"]
            missing_columns = []
            for i in required_columns:
                if i not in reader.fieldnames:
                    missing_columns.append(i)
            if len(missing_columns) > 0:
                raise Exception(', '.join(missing_columns) + " Column(s) does not exists")
            try:
                config = configparser.ConfigParser()
                config.read("constantsUpload.cnf")
                conn = mysql.connector.connect(host=config.get("client", "host"),
                                               database=config.get("client", "database"),
                                               user=config.get("client", "user"),
                                               password=config.get("client", "password"))
            except Error as e:
                error_arr.append(e.msg)

            if conn.is_connected():
                try:
                    cursor = conn.cursor(dictionary=True, buffered=True)

                    # Create the dictionary for all existing values of HR initials. HR initial as key and id as value.

                    cursor.execute("SELECT id,initial FROM hr")
                    hr_details = cursor.fetchall()
                    hr_map = dict()

                    for row in hr_details:
                        hr_map[row["initial"].lower()] = row["id"]

                    # Create the dictionary for all existing values of skills. skill name as key and skill id as value.
                    try:
                        cursor.execute("SELECT id,skill_name FROM skill")
                        skill_details = cursor.fetchall()
                        skill_map = dict()
                        for row in skill_details:
                            skill_map[row["skill_name"].lower()] = row["id"]
                    except Error as e:
                        error_arr.append(e.msg)

                    for row in reader:
                        # Checks for existing HR personals, if not exists then enter detail in database and store key
                        # value pair in hr_map dictionary.

                        try:
                            if row["CreatedBy"].lower().strip() not in hr_map:
                                cursor.execute("INSERT INTO hr (initial) VALUES (%s)", (row["CreatedBy"].strip(),))
                                hr_map[row["CreatedBy"].lower().strip()] = cursor.lastrowid

                            if row["UpdatedBy"].lower().strip() not in hr_map:
                                cursor.execute("INSERT INTO hr (initial) VALUES (%s)", (row["UpdatedBy"].strip(),))
                                hr_map[row["UpdatedBy"].lower().strip()] = cursor.lastrowid

                            # Insert employee details row by row.
                            cursor.execute("INSERT INTO employee (emp_id,first_name,last_name,created_by,updated_by) "
                                           "VALUES(%s,%s,%s,%s,%s)", (row["EmpID"].strip(), row["Name"].strip(),
                                                                      row["Last"].strip(),
                                                                      hr_map[row["CreatedBy"].lower().strip()],
                                                                      hr_map[row["UpdatedBy"].lower().strip()],))
                            current_emp_id = copy.copy(cursor.lastrowid)
                            # Insert Employee stack details.

                            cursor.execute("INSERT INTO employee_stack (emp_id,stack_id,nickname) VALUES (%s,%s,%s)",
                                           (current_emp_id, row["StackID"].strip(), row["StackNickname"].strip(),))
                            # Checks current skills in existing skills if not available then insert it into database and
                            # store key value pair in skill_map as skill name as key and id as value.
                            for key in sorted(row.keys()):
                                if re.match(r'Skill[0-9]+', key) and row[key].strip():
                                    if row[key].strip().lower() not in skill_map:
                                        cursor.execute("INSERT INTO skill (skill_name) VALUES (%s)",
                                                       (row[key].strip(),))
                                        skill_map[row[key].strip().lower()] = cursor.lastrowid
                                    cursor.execute(
                                        "INSERT INTO employee_skill (emp_id,skill_id) VALUES (%s,%s)",
                                        (current_emp_id, skill_map[row[key].strip().lower()],))
                            conn.commit()
                        except Error as e:
                            error_arr.append(e.msg)
                    print("Data Import Completed <br><br><br>")
                finally:

                    conn.close()
                    print("Errors occured during import <br><br>")
                    for i in error_arr:
                        print(i, "<br>")
    except KeyError:
        print("Check the submitted file <br>")
    except UnicodeDecodeError:
        print("Check the submitted file <br>")
    except Exception as e:
        print(e, "<br>", traceback.format_exc())
