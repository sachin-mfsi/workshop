#!/usr/bin/python3
import configparser  # To read constants file
import html  # To escape database data before parsing to html template
import os
import traceback  # To get traceback details of error

import mysql.connector
from mysql.connector import Error

if os.environ["REQUEST_METHOD"] == "GET":
    print("Content-Type: text/html;charset=utf-8\n")

    try:
        config = configparser.ConfigParser()
        config.read("constantsUpload.cnf")
        conn = mysql.connector.connect(host=config.get("client", "host"),
                                       database=config.get("client", "database"),
                                       user=config.get("client", "user"),
                                       password=config.get("client", "password"),
                                       charset='utf8', use_unicode=True)
    except Error as e:
        print(e)

    if conn.is_connected():
        try:
            query = "SELECT employee.emp_id, employee.first_name, employee.last_name, GROUP_CONCAT(skill.skill_name) AS skills, employee_stack.stack_id, employee_stack.nickname, hr.initial AS created_by, hr1.initial AS updated_by FROM employee LEFT JOIN employee_stack ON employee_stack.emp_id = employee.id JOIN employee_skill ON employee_skill.emp_id = employee.id JOIN skill ON employee_skill.skill_id = skill.id LEFT JOIN hr ON employee.created_by = hr.id LEFT JOIN hr AS hr1 ON employee.updated_by = hr1.id GROUP BY employee.id ORDER BY employee_skill.id"
            cursor = conn.cursor(dictionary=True, buffered=True)
            cursor.execute(query)
            data = cursor.fetchall()
            row_arr = []
            max_skill = max(map(len, [data[i]["skills"].split(',') for i in range(len(data))]))
            for row in data:
                row["skills"] = row["skills"].split(',')

                # Escaping data fetched from database.
                for key in row:
                    if key == 'skills':
                        for i in range(len(row[key])):
                            row[key][i] = html.escape(row[key][i])
                    else:
                        row[key] = html.escape(row[key])

                # Adding empty elements in skill array if size is less than maximum size.
                while len(row["skills"]) < max_skill:
                    row["skills"].append("")

                skill_columns = []
                for i in range(max_skill):
                    skill_columns.append(row["skills"][i])

                skill_columns = "</td><td>".join(skill_columns)

                # Creating table rows.

                data_string = "<tr><td>" + row["emp_id"] + "</td><td>" + row["first_name"] + "</td><td>" + row[
                    "last_name"] + "</td><td>" + skill_columns + "</td><td>" + row["stack_id"] + "</td><td>" + row[
                                  "nickname"] + "</td><td>" + \
                              row[
                                  "created_by"] + "</td><td>" + row["updated_by"]
                row_arr.append(data_string)
            final_data = ''.join(row_arr)
            skill_headers = []
            for i in range(max_skill):
                skill_headers.append("Skills" + str(i + 1))
            skill_headers = "</th><th>".join(skill_headers)

            f = open("./template/viewdata.html", encoding="utf-8")
            text = (f.read() % (skill_headers, final_data)).encode('ascii', 'xmlcharrefreplace')
            print(text.decode('utf-8'))
            f.close()
        except Exception as e:
            print(e, traceback.format_exc())
